package com.example.elastic.controllers;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.elasticsearch.NoSuchIndexException;
import org.springframework.data.elasticsearch.UncategorizedElasticsearchException;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/elastic")
public class ElasticBasicController {

    private final ElasticsearchOperations elasticsearchOperations;


    /**
     * Creates a new index in Elasticsearch. If an index with this name already exists, an exception is thrown.
     * The exception is handled by {@link com.example.elastic.configurations.GlobalExceptionHandler}
     *
     * @param indexName index name
     * @return 201 if index was created, 200 otherwise
     * @throws UncategorizedElasticsearchException if index with this name already exist.
     */
    @PutMapping("/{indexName}")
    public ResponseEntity<Void> createNewIndex(@PathVariable String indexName) throws UncategorizedElasticsearchException {
        return elasticsearchOperations.indexOps(IndexCoordinates.of(indexName)).create() ?
                ResponseEntity.status(HttpStatus.CREATED).build() :
                ResponseEntity.status(HttpStatus.OK).build();
    }

    /**
     * Create a new document in index. If the specified index does not exist, it will be created with the document.
     * The document ID is automatically generated, so this method will always create a new document instead of updating or overwriting it.
     *
     * @param indexName index name
     * @param map       any json object
     * @return created document along with its ID
     */
    @PostMapping(value = "/document/{indexName}", consumes = "application/json")
    public Map<String, ?> createNewDocument(@PathVariable String indexName, @RequestBody Map<String, ?> map) {
        return elasticsearchOperations.save(map, IndexCoordinates.of(indexName));
    }

    /**
     * Returns the document by its name and index, the response body may be empty if the document is not found.
     * The exception is handled by {@link com.example.elastic.configurations.GlobalExceptionHandler}
     *
     * @param indexName index name
     * @param id        document id
     * @return document or empty body
     * @throws NoSuchIndexException if index with this name not exist.
     */
    @SuppressWarnings("unchecked")
    @GetMapping(value = "/document/{indexName}/{id}")
    public Map<String, ?> getDocument(@PathVariable String indexName, @PathVariable String id) throws NoSuchIndexException {
        return elasticsearchOperations.get(id, Map.class, IndexCoordinates.of(indexName));
    }

}
