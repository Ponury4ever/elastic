package com.example.elastic.configurations;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.elc.ElasticsearchConfiguration;

/**
 * Simple connection setup with Elasticsearch.
 * Environment variables were intentionally used without default values.
 * This sample application uses the default elasticsearch settings, so connection with it is using TLS(via self-signed cert).
 * Due to this, I decided that configuring the client via environment variables would be easy to use.
 */
@Configuration
public class ElasticConfig extends ElasticsearchConfiguration {

    @Value("${elastic.uri}")
    private String uri;
    @Value("${elastic.username}")
    private String username;
    @Value("${elastic.password}")
    private String password;
    @Value("${elastic.cert.fingerprint}")
    private String fingerprint;

    @Override
    public ClientConfiguration clientConfiguration() {
        return ClientConfiguration.builder()
                .connectedTo(uri)
                .usingSsl(fingerprint)
                .withBasicAuth(username, password)
                .build();
    }

}
