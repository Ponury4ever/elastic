package com.example.elastic.configurations;

import org.springframework.data.elasticsearch.NoSuchIndexException;
import org.springframework.data.elasticsearch.UncategorizedElasticsearchException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@RestControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = UncategorizedElasticsearchException.class)
    protected ResponseEntity<Object> handle(UncategorizedElasticsearchException ex) {
        return ResponseEntity
                .status(ex.getStatusCode() != null ? ex.getStatusCode() : HttpStatus.FORBIDDEN.value())
                .body(ex.getResponseBody());
    }

    @ExceptionHandler(value = NoSuchIndexException.class)
    protected ResponseEntity<Object> handle(NoSuchIndexException ex) {
        return ResponseEntity
                .status(HttpStatus.CONFLICT)
                .body(ex.getMessage());
    }


}
